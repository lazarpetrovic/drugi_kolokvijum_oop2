#include "deceleratingsprite.h"

DeceleratingSprite::DeceleratingSprite(SpriteSheet *sheet, int width, int height) : Sprite(sheet, width, height) {

}
void DeceleratingSprite::draw(SDL_Renderer *renderer){
	if(state&LEFT) {
	        sheet->drawFrame("walk_left", currentFrame, spriteRect, renderer);
	    } else if(state&RIGHT) {
	        sheet->drawFrame("walk_right", currentFrame, spriteRect, renderer);
	    } else if(state&UP) {
	        sheet->drawFrame("walk_up", currentFrame, spriteRect, renderer);
	    } else if(state&DOWN) {
	        sheet->drawFrame("walk_down", currentFrame, spriteRect, renderer);
	    } else if(state == STOP) {
	        sheet->drawFrame("walk_down", 0, spriteRect, renderer);
	    }

	    frameCounter++;
	    if(frameCounter%frameSkip == 0) {
	        currentFrame++;
	        if(currentFrame >= 9) {
	            currentFrame = 0;
	        }
	        frameCounter = 0;
	    }
	}

void DeceleratingSprite::move(int dx, int dy) {
	if(acceleration > minAcceleration){
		acceleration -=1;
	}
	if (acceleration == minAcceleration) {
		spriteRect->x += acceleration;
	}

    spriteRect->x += dx;
    spriteRect->y += dy;
    cout << "Trenutna brzina kretanja je: " << acceleration << endl;
}

void DeceleratingSprite::move() {
	if(state != 0) {
		if(state & 1) {
			move(0, 0);
		}
		if(state & 2) {
			move(0, 0);
		}
		if(state & 4) {
			move(0, 0);
		}
		if(state & 8) {
			move(0, 0);
		}
		if(state & 1 and state & 4){
			move(-1, -1);

		}
		if(state & 1 and state & 8){
			move(-1, 1);

		}
		if(state & 2 and state & 4){
			move(1, -1);

		}
		if(state & 2 and state & 8){
			move(1, 1);

		}
	}
}

DeceleratingSprite::~DeceleratingSprite() {
	// TODO Auto-generated destructor stub
}
