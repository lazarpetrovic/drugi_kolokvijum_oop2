#include <string>
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <iostream>
#include "keyboardeventlistener.h"
#include "sprite.h"

#ifndef DECELERATINGSPRITE_H_
#define DECELERATINGSPRITE_H_

class DeceleratingSprite : public Sprite {
public:
    int acceleration = 500;
    int minAcceleration = 1;
    DeceleratingSprite(SpriteSheet *sheet, int width=64, int height=64);
    virtual ~DeceleratingSprite();
    virtual void draw(SDL_Renderer *renderer);
    virtual void move(int dx, int dy);
    virtual void move();
};
#endif // DECELERATINGSPRITE_H_
