#include <string>
#include <iostream>
#include <fstream>
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <vector>
#include "player.h"
#include "deceleratingsprite.h"
#include "spritesheet.h"
#include "engine.h"

#ifndef GROUP_H_
#define GROUP_H_

class Player;
class Engine;
class Group {
public:
	int maxSize = 3;
	vector<DeceleratingSprite*>decelerations;
	Group(vector<DeceleratingSprite*>decelerations);
	virtual ~Group();
	virtual void load(istream &inputStream, SpriteSheet *sheet, Engine *engine);
	virtual void save(ostream &outputStream);
};

#endif /* GROUP_H_ */
