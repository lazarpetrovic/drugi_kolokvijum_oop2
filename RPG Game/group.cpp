#include "group.h"

Group::Group(vector<DeceleratingSprite*>decelerations) {
	this->decelerations = decelerations;
}

Group::~Group() {
	// TODO Auto-generated destructor stub
}

void Group::load(istream &inputStream, SpriteSheet *sheet, Engine *engine){
	inputStream >> maxSize;
		for (int i=0; i < maxSize; i++) {
			DeceleratingSprite *k = new DeceleratingSprite(sheet);
			Player *player = new Player(k);
			inputStream >> k->spriteRect->x >> k->spriteRect->y;
			engine->drawables.push_back(k);
			engine->movables.push_back(k);
			engine->eventListeners.push_back(player);
			decelerations.push_back(k);
			}
	}

void Group::save(ostream &outputStream){
	outputStream << decelerations.size() << " ";
	for(DeceleratingSprite *i : decelerations){
		outputStream << i->spriteRect->x << " " << i->spriteRect->y << " ";
	}
}

